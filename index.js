const express = require('express');
const mongoose = require('mongoose');
const PORT = process.env.PORT || 8080;
const authRouter = require('./routes/auth');
const userRouter = require('./routes/user');
const truckRouter = require('./routes/truck');
const loadRouter = require('./routes/load');
const app = express();

app.use(express.json());
app.use("/api", authRouter);
app.use('/api', userRouter);
app.use('/api', truckRouter);
app.use('/api', loadRouter);

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://exiio:wewewe1234@cluster0.f43tc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority');
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
    } catch (e) {
        console.log(e);
    }
}

start();
