const {Router} = require('express');
const userRouter = new Router();
const userController = require('../controllers/userController');
const authMiddleWare = require('../middleware/authMiddleware');
const {check} = require('express-validator');

userRouter.get('/users/me', authMiddleWare, userController.getUser);

userRouter.delete('/users/me', authMiddleWare, userController.deleteUser);

userRouter.patch('/users/me/password', [
    check('password', 'Password must be long 4 and short 10 symbols')
        .isLength({min: 4, max: 10}),
], authMiddleWare, userController.changePassword);

module.exports = userRouter;
