const {Router} = require('express');
const loadRouter = new Router();
const loadController = require('../controllers/loadController');
const authMiddleWare = require('../middleware/authMiddleware');
const roleMiddleware = require('../middleware/roleMiddleware');

loadRouter.get('/loads', authMiddleWare, roleMiddleware(["SHIPPER", "DRIVER"]), loadController.getLoads);

loadRouter.post('/loads', authMiddleWare, roleMiddleware(["SHIPPER"]), loadController.addLoad);

loadRouter.get('/loads/active', roleMiddleware(["DRIVER"]), loadController.getLoadActive);

loadRouter.patch('/loads/active/state', roleMiddleware(["DRIVER"]), loadController.patchLoad);

loadRouter.get('/loads/:id', roleMiddleware(["SHIPPER", "DRIVER"]), loadController.getLoad);

loadRouter.put('/loads/:id', roleMiddleware(["SHIPPER"]), loadController.updateLoad);

loadRouter.delete('/loads/:id', roleMiddleware(["SHIPPER"]), loadController.deleteLoad);

loadRouter.post('/loads/:id/post', authMiddleWare, roleMiddleware(["SHIPPER"]), loadController.postLoad);

loadRouter.get('/loads/:id/shipping_info', roleMiddleware(["SHIPPER"]), loadController.getLoadShippingInfo);

module.exports = loadRouter;
