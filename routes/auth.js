const Router = require('express');
const authRouter = new Router();
const authController = require('../controllers/authController');
const {check} = require("express-validator");
const authMiddleware = require('../middleware/authMiddleware');
const roleMiddleware = require('../middleware/roleMiddleware');

authRouter.post('/auth/register', [
    check('email', "Email пользователя не может быть пустым").notEmpty(),
    check('password', "Пароль должен быть больше 4 и меньше 10 символов").isLength({min:4, max:10})
], authController.registration);
authRouter.post('/auth/login', authController.login);
authRouter.get('/users', authMiddleware, authController.getUsers);

module.exports = authRouter;
