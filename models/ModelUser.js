const {Schema, model} = require('mongoose');

const User = new Schema({
    email: {type: String, unique: true, required: true},
    password: {type: String, required: true},
    role: [{type: String, ref: 'Role'}]
}, {versionKey: false});

module.exports = model('ModelUser', User);
