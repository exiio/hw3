const {Schema, model} = require('mongoose');

const Load = new Schema({
    created_by: {type: Schema.Types.ObjectId, ref: 'User'},
    assigned_to: {type: Schema.Types.ObjectId, ref: 'User'},
    status: {type: String, required: true, enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'], default: 'NEW'},
    state: {type: String, required: true, enum:
            [ 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery' ], default: 'En route to Pick Up'},
    name: {type: String, required: true},
    payload: {type: Number, required: true},
    pickup_address: {type: String, required: true},
    delivery_address: {type: String, required: true},
    dimensions: {
        width: {type: Number, required: true},
        length: {type: Number, required: true},
        height: {type: Number, required: true}
    },
    logs: {
        message: {type: String, default: 'Load assigned to driver with'},
        time: {type: Date, default: Date.now()}
    },
    created_date: {type: Date, default: Date.now()},
}, {versionKey: false});

module.exports = model('ModelLoad', Load);
